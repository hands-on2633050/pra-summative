public class Day5Task123 extends Day5Task122 {
    public String name = "Charmeleon";

    @Override
    public void announce() {
        System.out.print("Saya adalah " + name +  this.oldEvo() + " ");
    }

    public String oldEvo() {
        return "Evolusi sebelumnya adalah " + super.name + ". ";
    }
    
}
