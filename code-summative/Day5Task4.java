public class Day5Task4 {
    public static void main(String[] args) {
        int loops = 10;

        if(args.length > 0){
            loops = Integer.parseInt(args[0]);
        }

        System.out.println("Launch in...");
        for(int i = loops; i > 0; i--){
            System.out.print(i);
            load();
        }
        System.out.println("LAUNCH!");
    }

    public static void load(){
        for(int j = 0; j < 10; j++){
            try {
                System.out.print(". ");
                Thread.sleep(100);
            } catch (Exception e) {
            }
        }
        System.err.print("\n");
    }
}
