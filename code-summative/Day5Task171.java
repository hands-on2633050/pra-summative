public class Day5Task171 {
    public static void main(String[] args) {
        Mario mario = new Mario();

        mario.greet();
        mario.jump();
    }

    static class Mario implements Jumpable, Greetable {
        @Override
        public void jump() {
            System.out.println("YAHOOO");
        }

        @Override
        public void greet() {
            System.out.println("Hey, it's-a-me, Mario!");
        }
    }

    static interface Jumpable {
        public void jump();
    }

    static interface Greetable {
        public void greet();
    }
}
