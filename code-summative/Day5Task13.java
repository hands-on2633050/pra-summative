public class Day5Task13 {
    public static void main(String[] args) {
        Greeter defaultGreeter = new Greeter();
        Greeter glados = new Greeter("GLaDOS");
        Greeter lemongrab = new Greeter("Lemongrab", true);

        defaultGreeter.greet();
        glados.greet();
        lemongrab.greet(true);
    } 

    static class Greeter {
        private String name;
        Greeter(){
            this.name = "Greeter";
        }

        Greeter(String name){
            this.name = name;
        }

        Greeter(String name, boolean announce){
            this.name = name;
            System.out.println(name + " IS ALIVE!");
        }

        public void greet(){
            System.out.println("Hello, my name is " + name);
        }

        public void greet(boolean loud){
            System.out.println(("Hello, my name is " + name).toUpperCase());
        }
    }
}
