public class Day5Task151 {
    public static void main(String[] args) {
        Batman batman = new Batman();
        Robin robin = new Robin();

        batman.fight();

        robin.fight();
    }

    static class Batman {
        public void fight(){
            System.out.println("KER-POW!");
        }
    }

    static class Robin extends Batman {
        @Override
        public void fight() {
            super.fight();
            System.out.println("Cing-cing-cing!");
        }
    }
}
