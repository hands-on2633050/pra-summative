import java.util.HashMap;
import java.util.Scanner;

public class Day5Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        HashMap<String, Integer> questionsAndAnswers = new HashMap<String, Integer>();

        questionsAndAnswers.put("Hari Kartini tanggal berapa? ", 21);
        questionsAndAnswers.put("Hari pahlawan tanggal berapa? ", 10);
        questionsAndAnswers.put("Berapa warna bendera Indonesia? ", 2);
        questionsAndAnswers.put("Tahun berapa Indonesia merdeka? ", 1945);
        questionsAndAnswers.put("Kapan hari kemerdekaan? ", 17);

        int score = 0;

        System.out.println("Jawab pertanyaan berikut dengan angka.\n\n");
        for(String question : questionsAndAnswers.keySet()){
            System.out.print(question);
            int answer = scanner.nextInt();
            if(answer == questionsAndAnswers.get(question)){
                score++;
                System.out.println("Benar!");
            }else {
                System.out.println("Salah!");
            }
        }
        System.out.println("Score: " + score);

        System.out.println("Your grade: " + ((double) score/questionsAndAnswers.size()) * 100 + "%");

        scanner.close();

    }
}
