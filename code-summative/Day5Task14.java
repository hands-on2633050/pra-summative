public class Day5Task14 {
    public static void main(String[] args) {
        Dragon dragon = new Dragon();
        DragonBorn dragonborn = new DragonBorn();

        dragon.shout();

        dragonborn.shout();
    }

    static class Dragon {
        public void shout(){
            System.out.println("YOOR TUL SHUL!");
        }
    }

    static class DragonBorn extends Dragon {
        @Override
        public void shout() {
            System.out.println("FUS RO DAH!");
        }
    }
}
