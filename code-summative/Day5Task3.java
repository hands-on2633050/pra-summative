import java.util.Scanner;

public class Day5Task3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Warna kesukaan? ");
        String answer = scanner.nextLine();
        
        String response;
        switch (answer.toLowerCase()) {
            case "merah":
                response = "Warna darahku itu.";
                break;
            case "putih":
                response = "Warna tulangku itu.";
                break;
            default:
                response = "Warna apa itu? ";
                break;
        }

        System.out.println(response);
        scanner.close();

    }
}
