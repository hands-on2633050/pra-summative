public class Day5Task1 {

    // Instance variables
    private int instanceVar1;
    private String instanceVar2;

    // Static variable
    private static double staticVar;

    // Constructor, initialisasi instance variables
    public Day5Task1(int instanceVar1, String instanceVar2) {
        this.instanceVar1 = instanceVar1;
        this.instanceVar2 = instanceVar2;
    }

    // Metode dengan local variable
    public void method() {
        // Local variable
        int localVar = 42;

        System.out.println("Instance Variable 1: " + instanceVar1);
        System.out.println("Instance Variable 2: " + instanceVar2);
        System.out.println("Static Variable: " + staticVar);
        System.out.println("Local Variable: " + localVar);
    }


    public static void main(String[] args) {
        Day5Task1 instance = new Day5Task1(10, "Bro bro");

        instance.method();
    }
}
