function Buah(name) {
  this.name = name;
  this.getName = function () {
    return this.name;
  };
}

const fruits = {
  pisang: new Buah("Pisang"),
  mangga: new Buah("Mangga"),
  anggur: new Buah("Anggur"),
};

for (const key in fruits) {
  console.log(fruits[key].getName());
}
