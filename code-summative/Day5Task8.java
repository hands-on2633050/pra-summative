import java.util.ArrayList;
import java.util.Scanner;

public class Day5Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<String> favoriteGames = new ArrayList<String>();

        System.out.println("List your favorite games. Enter to add and 'q' to quit.");
        
        String answer = scanner.nextLine();

        while(!answer.toLowerCase().equals("q")){
                favoriteGames.add(answer);
                answer = scanner.nextLine();
        }

        System.out.println("\n====================\nHere are your favorite games.");

        for (String game : favoriteGames) {
            System.out.println(game);
        }

        scanner.close();
    }
}
