public class Day5Task2 {
    public static void main(String[] args) {
        System.out.println("Are you lucky?");
        
        boolean areYouLucky = Math.random() >= 0.5;

        System.out.println(areYouLucky ? "Congrats. Fortune smiles upon you." : "You are a loser.");
    }
}
