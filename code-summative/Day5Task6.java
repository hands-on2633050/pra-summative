public class Day5Task6 {
    public static void main(String[] args) {
        System.out.println("If you can correctly count to ten, you win a prize!");
        for(int i = 1; i <= 10; i++){
            if(i == 6) continue;
            System.out.print(i);
            load();

        }
        System.out.println("Sorry! You missed 6!");


    }

    public static void load(){
        for(int j = 0; j < 10; j++){
            try {
                System.out.print(". ");
                Thread.sleep(100);
            } catch (Exception e) {
            }
        }
        System.err.print("\n");
    }
}
