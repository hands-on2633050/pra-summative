public class Day5Task124 extends Day5Task123 {
    public String name = "Charizard";

    @Override
    public void announce() {
        System.out.println("Saya adalah " + name + " " +   this.oldEvo());
    }

    @Override
    public String oldEvo() {
        return "Evolusi sebelumnya adalah " + super.name + " " + super.oldEvo();
    }

}
