import java.util.Scanner;

public class Day5Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] movies = {
            "Shawshank Rdemeption", 
            "Saving Private Ryan", 
            "Three Billboards Outside Ebbing, Missouri",
            "The Terminal",
            "Captain America: Winter Soldier"
        };

        int[] ratings = new int[movies.length];

        System.out.println("How would you rate these movies? (1 - 10)");
        for(int i = 0; i < movies.length; i++){
            System.out.print(movies[i] + ": ");
            int rating = scanner.nextInt();
            rating = rating > 10 ? 10 : rating < 1 ? 1 : rating;
            ratings[i] = rating;
        }

        System.out.println("\nYour ratings:\n==================");
        for(int i = 0; i < movies.length; i++){
            System.out.print(movies[i] + ": " + "*".repeat(ratings[i]));
            System.out.println();
        }
        scanner.close();

        
    }
}
