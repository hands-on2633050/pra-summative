public class Day5Task161 {
    public static void main(String[] args) {
        Avenger[] avengers = {new CaptainAmerica(), new IronMan(), new Hulk()};
        for (Avenger avenger : avengers) {
            avenger.assemble();
        }

    }

    static class Avenger {
        public void assemble(){
            System.out.println("Avengers! Assemble!");
        }
    }

    static class CaptainAmerica extends Avenger {
        @Override
        public void assemble() {
            System.out.println("I can do this all day!");
        }
    }

    static class IronMan extends Avenger {
        @Override
        public void assemble() {
            System.out.println("Not a great plan!");
        }
    }

    static class Hulk extends Avenger {
        @Override
        public void assemble() {
            System.out.println("Puny God!");
        }
    }
}
